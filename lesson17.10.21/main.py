from sys import addaudithook
from pygraph.graph import *


windowSize(1200, 800)
canvasSize(1200, 800)

def printChimney(x, y, xSize=30, ySize=60):
    penSize(4)
    brushColor("firebrick")
    mainPartW = xSize*0.8
    mainPartH = y+ySize
    mainPartX = x + mainPartW
    mainPartY = y + mainPartH
    rectangle(x, y, x+xSize, y+ySize)
    brickH = ySize/18
    rectangle(x-xSize/3, y-brickH, x+xSize+xSize/3, y+brickH)

def roof(x, y, w, h):
    brushColor("firebrick")
    polygon([(x, y), (x+w/2, y+h), (x-w/2, y+h)])

def draw_bush(x,y,):
    brushColor(50, 205, 5,)
    polygon([(x-125, y), (x, y-200), (x+125, y)])
    line(x,y-200,x,y)
    line(x,y-25,x-50,y-75)
    line(x,y-50,x+50,y-100)
    brushColor(200,50,50)
    rectangle(x-15,y,x+15,y+100)

def wall(x, y, w, h):
    penSize(4)
    brushColor(255, 215, 0)
    rectangle(x, y, x+w, y+h)


def door(x, y, w, h):
    brushColor(50, 205, 5,)
    rectangle(x, y, x+w, y+h)
    delta = int(h/10)
    for i in range(y, y+h, delta):
        line(x, i, x+w, i+delta)

def wall_window(x, y, w, h):
    penSize(4)
    brushColor(50, 50, 100)
    rectangle(x, y, x+w, y+h)
    line(x+w/2, y, x+w/2, y+h)
    line(x, y+h/2, x+w, y+h/2)

def roof_window(x, y, r):
    brushColor(50, 50, 100)
    circle(x, y, r)
    line(x-r, y, x+r, y)
    line(x, y-r, x, y+r)


def house(x, y, w, h):
    chimneyX = x+w*0.75
    chimneyY = y+h*0.1
    chimneyW = w*0.1
    chimneyH = h*0.20
    printChimney(chimneyX, chimneyY, chimneyW, chimneyH)
    wall_h = h/3*2
    wall_w = w-w/10
    wall_x = x+w/20
    wall_y = y+h/3
    wall(x=wall_x, y=wall_y, w=wall_w, h=wall_h)
    # door(525, 450, 150, 300)
    roof_x = x+w/2
    roof_y = y
    roof_w = w
    roof_h = h/3
    roof(x=roof_x, y=roof_y, w=roof_w, h=roof_h)

    wall_window_x = wall_x + wall_w/10
    wall_window_y = wall_y + wall_h/3
    wall_window_w = wall_w/5
    wall_window_h = wall_h/3
    window2_x = wall_x + wall_w - wall_w/10 - wall_window_w
    wall_window(x=wall_window_x, y=wall_window_y,
                w=wall_window_w, h=wall_window_h)
    wall_window(x=window2_x, y=wall_window_y,
                w=wall_window_w, h=wall_window_h)
    door_w = wall_w/5
    door_h = wall_h * (2/3)
    door_x = wall_x+wall_w/2 - door_w/2
    door_y = wall_y + wall_h * (1/3)
    door(x=int(door_x), y=int(door_y), w=int(door_w), h=int(door_h))
    r_window_x = roof_x
    r_window_y = roof_y+roof_h/2
    r_window_r = wall_w/15
    roof_window(x=r_window_x, y=r_window_y, r=r_window_r)
    draw_bush(175,700)

if __name__ == "__main__":
    canvas_w = 1200
    canvas_h = 800
    windowSize(canvas_w, canvas_h)
    canvasSize(canvas_w, canvas_h)

    house(x=canvas_w/4, y=canvas_h/4, w=800, h=600)

    run()
    